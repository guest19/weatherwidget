window.onload = async function () {

    // сохранение данных в хранилище по имени
    const setLocalstorageData = (whereSave_itemName, dataForSave) => {
        localStorage.setItem(whereSave_itemName, JSON.stringify(dataForSave));
    }
    // получени данных из хранилища по имени
    const getLocalstorageData = (nameItem) => {
        return JSON.parse(localStorage.getItem(nameItem));
    }

    let units = 'metric';
    let exclude = 'alerts,daily,hourly,minutely';
    let land = 'ru';
    let appid = '8f8cc921edc295b9de66dbb4bac5165f';
    let cityList = getLocalstorageData('cityList');
    let dragCity = '';
    let warningMessage = '';
    let isConfigured = false;

    // создание дом элемента
    const createDomElement = (elementOptions) => {
        let DOMelement = null;
        for (let key1 in elementOptions) {
            DOMelement = document.createElement(key1);
            for (let key2 in elementOptions[key1]) {
                if (key2 === 'style') {
                    for (let styleName in elementOptions[key1][key2]) {
                        DOMelement.style[styleName] = elementOptions[key1][key2][styleName];
                    }
                }

                if (key2 === 'setAttribute') {
                    for (let attributeName in elementOptions[key1][key2]) {
                        DOMelement.setAttribute(attributeName, elementOptions[key1][key2][attributeName]);
                    }
                }
                if (key2 === 'classList') {
                    for (let className of elementOptions[key1][key2]) {
                        DOMelement.classList.add(className);
                    }
                }

                if (key2 === 'innerHTML') {
                    DOMelement[key2] = elementOptions[key1][key2];

                }
            }
        }
        return DOMelement;
    }
    // получение дом элемента
    const getDomElement = (itemSelector) => {
        return document.querySelector(itemSelector);
    }

    // main блок
    let widget = getDomElement('weather-widget');
    widget.style.width = '200px';
    widget.style.boxSizing = 'border-box';
    widget.style.position = 'relative';

    // =====add-fontawesome=====
    let fontawesomeOptions = {
        script: {
            setAttribute: {
                src: 'https://kit.fontawesome.com/c0e50f1ced.js',
                crossorigin: 'anonymous'
            }
        }
    }
    let fontawesome = createDomElement(fontawesomeOptions);
    widget.appendChild(fontawesome);

    // =====add-jquery=====
    let jqueryOptions = {
        script: {
            setAttribute: {
                src: 'https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.js',
            }
        }
    }
    let jquery = createDomElement(jqueryOptions);
    widget.appendChild(jquery);

    // вывод сообщения
    let renderMessageBlock = async (message, timeOut = 0) => {

        let createBlock = (text) => {
            let blockOptions = {
                div: {
                    style: {
                        position: 'absolute',
                        display: 'flex',
                        alignItems: 'center',
                        width: '200px',
                        border: '1px solid red',
                        borderRadius: '25px 25px 25px 0',
                        background: 'rgba(173, 255, 47, 0.9)',
                        top: '0',
                        right: '-103%',
                        color: 'red',
                        padding: '5px 10px',
                    },
                    setAttribute: {
                        id: 'messageBlock'
                    },
                    innerHTML: text
                },
            }
            let block = createDomElement(blockOptions);
            return block;
        }

        if (message !== '' && timeOut === 0) {
            await deleteBlock("#messageBlock", "weather-widget");
            let messageBlock = await createBlock(message);
            await widget.appendChild(messageBlock);

        } else if (message !== '' && timeOut >= 1) {
            await deleteBlock("#messageBlock", "weather-widget");
            let messageBlock = await createBlock(message);
            await widget.appendChild(messageBlock);

            await setTimeout(() => {
                deleteBlock("#messageBlock", "weather-widget");
            }, timeOut * 1000);

        } else {
            await deleteBlock("#messageBlock", "weather-widget");
        }
    }

    // проверка - был ли настроен виджет
    let verificationIsConfigured = async () => {
        isConfigured = await getLocalstorageData('isConfigured');
        if (!isConfigured) {
            warningMessage = "Вы можете настроить виджет один раз!";
            await renderMessageBlock(warningMessage);
        } else {
            await deleteBlock('#subscribe_form', "#weather-widgetMenu")
            warningMessage = "Вы уже настраивали виджет. Настройки более не доступны на этом устройстве";
        }
    }

    // удаление блока домэлемента
    let deleteBlock = (searchBlockId, fromBlock) => {
        searchBlockId = getDomElement(searchBlockId)
        fromBlock = getDomElement(fromBlock)
        if (searchBlockId) {
            fromBlock.removeChild(searchBlockId);
        }
    }

    // отрисовка блока данных
    const renderCard = async (cityName) => {
        deleteBlock('#weather-widget__content', "weather-widget")

        const createContentBlock = (dataInfo) => {
            // deleteBlock('#weather-widget__content', "weather-widget")
            let weather_widget__contentOptions = {
                div: {
                    style: {
                        display: 'flex',
                        width: '200px',
                        boxSizing: 'border-box',
                        flexDirection: 'column',
                        justifyContent: 'center',
                        alignItems: 'center',
                        padding: '10px',
                        border: '1px solid red',
                        borderRadius: '10px',
                        boxShadow: '0px 5px 10px 2px rgba(34, 60, 80, 0.2)',
                        webkitBoxShadow: '0px 5px 10px 2px rgba(34, 60, 80, 0.2)',
                        fontFamily: 'Arial',
                        fontSize: '12px',
                    },
                    setAttribute: {
                        id: 'weather-widget__content'
                    }
                }
            }
            let weather_widget__content = createDomElement(weather_widget__contentOptions);


            // =====city_name=====
            let city_nameOptions = {
                div: {
                    style: {
                        display: 'flex',
                        justifyContent: 'space-between',
                        width: '100%',
                        fontWeight: 'bold',
                    }
                },
            }
            let city_name = createDomElement(city_nameOptions);

            let city_name_pOptions = {
                p: {
                    style: {
                        margin: '0',
                    }
                },
            }
            let city_name_p = createDomElement(city_name_pOptions);
            if (dataInfo.name) {
                city_name_p.innerHTML = `${dataInfo.name}, ${dataInfo.sys.country}`;
            }
            if (dataInfo.message) {
                city_name_p.innerHTML = `${dataInfo.message}`;
            }
            city_name.appendChild(city_name_p);

            // кнопка открытия настроек
            let city_name_i_Options = {
                i: {
                    style: {
                        fontSize: '16px',
                        cursor: 'pointer',
                    },
                    setAttribute: {
                        "aria-hidden": "true",
                        id: "widgetMenu_openBtn"
                    },
                    classList: ['fa', 'fa-cog']
                },
            }
            let city_name_i = createDomElement(city_name_i_Options);
            city_name.appendChild(city_name_i);

            // сворачивания окна показа данных
            city_name_i.addEventListener('click', async () => {
                renderSetting()
                let weather_widgetMenu = await document.getElementById('weather-widgetMenu');
                weather_widget__content.style.display = 'none';
                weather_widgetMenu.style.display = 'flex';
            })
            weather_widget__content.appendChild(city_name);
            return weather_widget__content
        }

        await fetch(`https://api.openweathermap.org/data/2.5/weather?q=${cityName}&exclude=${exclude}&appid=${appid}`)
            .then(function (resp) {
                let respFormat = resp.json() // convert data to js
                return respFormat;

            })
            .then(function (dataJsonCity) {

                const weather_widget__content = createContentBlock(dataJsonCity);
                deleteBlock("#weather-widget__content", "weather-widget")
                widget.appendChild(weather_widget__content);

                // получение данных по координатам города
                fetch(`https://api.openweathermap.org/data/2.5/onecall?lat=${dataJsonCity.coord.lat}&lon=${dataJsonCity.coord.lon}&units=${units}&exclude=${exclude}&appid=${appid}`, dataJsonCity)

                    .then(function (resp) {
                        let dataJson = resp.json() // convert data to js
                        return dataJson
                    })
                    .then(function (dataJson) {

                        let cityList = JSON.parse(localStorage.getItem('cityList'))
                        cityList.forEach((item) => {
                            if (item.cityName === cityName) {
                                item.details = dataJson;
                                dataJson = item;
                            }
                        })

                        let weather_widget__content = document.getElementById('weather-widget__content');
                        // =====hr=====
                        let hr_Options = {
                            hr: {}
                        }
                        let hr = createDomElement(hr_Options);
                        weather_widget__content.appendChild(hr);

                        // =====temperatureBox=====
                        let temperatureBox_options = {
                            div: {
                                style: {
                                    display: "flex",
                                    alignItems: "center",
                                }
                            }
                        }
                        let temperatureBox = createDomElement(temperatureBox_options);

                        let temperatureBox_img_options = {
                            img: {
                                setAttribute: {
                                    src: `https://openweathermap.org/img/wn/${dataJson.details.current.weather[0].icon}@2x.png`,
                                }
                            }
                        }
                        let temperatureBox_img = createDomElement(temperatureBox_img_options);
                        temperatureBox.appendChild(temperatureBox_img);


                        let temperatureBox_p_options = {
                            p: {
                                style: {
                                    fontSize: `30px`,
                                },
                                innerHTML: `${Math.round(dataJson.details.current.temp)}°C`
                            }
                        }
                        let temperatureBox_p = createDomElement(temperatureBox_p_options);
                        temperatureBox.appendChild(temperatureBox_p);
                        weather_widget__content.appendChild(temperatureBox);


                        // =====temperatureText=====
                        let temperatureText_options = {
                            div: {
                                style: {
                                    width: "100%",
                                    display: "flex",
                                    justifyContent: "space-between",
                                }
                            }
                        }
                        let temperatureText = createDomElement(temperatureText_options);

                        let temperatureText_p1_options = {
                            p: {
                                style: {
                                    fontSize: "12px",
                                    textAlign: "center",
                                },
                                innerHTML: `Feels like ${Math.round(dataJson.details.current.feels_like)}°C`
                            }
                        }
                        let temperatureText_p1 = createDomElement(temperatureText_p1_options);

                        let temperatureText_p2_options = {
                            p: {
                                style: {
                                    fontSize: "12px",
                                    textAlign: "center",
                                    textTransform: "capitalize",
                                },
                                innerHTML: dataJson.details.current.weather[0].main
                            }
                        }
                        let temperatureText_p2 = createDomElement(temperatureText_p2_options);

                        let temperatureText_p3_options = {
                            p: {
                                style: {
                                    fontSize: "12px",
                                    textAlign: "center",
                                    textTransform: "capitalize",
                                },
                                innerHTML: dataJson.details.current.weather[0].description
                            }
                        }
                        let temperatureText_p3 = createDomElement(temperatureText_p3_options);

                        temperatureText.appendChild(temperatureText_p1);
                        temperatureText.appendChild(temperatureText_p2);
                        temperatureText.appendChild(temperatureText_p3);
                        weather_widget__content.appendChild(temperatureText);

                        // =====wind=====
                        let temperatureWind_options = {
                            div: {
                                style: {
                                    width: "100%",
                                    display: "flex",
                                    justifyContent: "space-around",
                                }
                            }
                        }
                        let temperatureWind = createDomElement(temperatureWind_options);

                        let temperatureWind_p1_options = {
                            p: {
                                style: {
                                    display: "flex",
                                    alignItems: "center",
                                },
                            }
                        }
                        let temperatureWind_p1 = createDomElement(temperatureWind_p1_options);

                        let temperatureWind_p1_svg_options = {
                            svg: {

                                setAttribute: {
                                    "data-v-47880d39": "",
                                    "viewBox": "0 0 1000 1000",
                                    "enable-background": "new 0 0 1000 1000",
                                    "xml:space": "preserve",
                                    "style": `transform: rotate(${dataJson.details.current.wind_deg}deg);`,
                                    "height": "12pt",
                                    "margin-right": "2pt",
                                },
                                innerHTML: ' <g data-v-47880d39="" fill="#48484a">\n' +
                                    '                    <path data-v-47880d39="" d="M510.5,749.6c-14.9-9.9-38.1-9.9-53.1,1.7l-262,207.3c-14.9,11.6-21.6,6.6-14.9-11.6L474,48.1c5-16.6,14.9-18.2,21.6,0l325,898.7c6.6,16.6-1.7,23.2-14.9,11.6L510.5,749.6z"></path>\n' +
                                    '                    <path data-v-47880d39="" d="M817.2,990c-8.3,0-16.6-3.3-26.5-9.9L497.2,769.5c-5-3.3-18.2-3.3-23.2,0L210.3,976.7c-19.9,16.6-41.5,14.9-51.4,0c-6.6-9.9-8.3-21.6-3.3-38.1L449.1,39.8C459,13.3,477.3,10,483.9,10c6.6,0,24.9,3.3,34.8,29.8l325,898.7c5,14.9,5,28.2-1.7,38.1C837.1,985,827.2,990,817.2,990z M485.6,716.4c14.9,0,28.2,5,39.8,11.6l255.4,182.4L485.6,92.9l-267,814.2l223.9-177.4C454.1,721.4,469,716.4,485.6,716.4z"></path>\n' +
                                    '                </g>'
                            }
                        }
                        let temperatureWind_p1_svg = createDomElement(temperatureWind_p1_svg_options);

                        temperatureWind_p1.appendChild(temperatureWind_p1_svg);

                        temperatureWind_p1.innerHTML += `&nbsp; ${Math.round((dataJson.details.current.wind_speed + Number.EPSILON) * 100) / 100}m/s`

                        let temperatureWind_p2_options = {
                            p: {
                                style: {
                                    display: "flex",
                                    alignItems: "center",
                                },
                            }
                        }
                        let temperatureWind_p2 = createDomElement(temperatureWind_p2_options);

                        let temperatureWind_p2_svg_options = {
                            svg: {
                                style: {
                                    height: "16pt",
                                    width: "auto",
                                    marginRight: "2pt",
                                    fontSize: "12pt",
                                },
                                setAttribute: {
                                    "data-v-7bdd0738": "",
                                    "data-v-3208ab85": "",
                                    "height": "96pt",
                                    "width": "96px",
                                    "viewBox": "0 0 96 96",
                                    "margin-right": "2pt",
                                },
                                innerHTML: `<g data-v-7bdd0738=""
                   transform="translate(0,96) scale(0.100000,-0.100000)"
                   fill="#48484a"
                   stroke="none"
                >
                    <path data-v-7bdd0738="" d="M351 854 c-98 -35 -179 -108 -227 -202 -27 -53 -29 -65 -29 -172 0
                -107 2 -119 29 -172 38 -75 104 -141 180 -181 58 -31 66 -32 176 -32 110 0
                118 1 175 32 77 40 138 101 178 178 31 57 32 65 32 175 0 110 -1 118 -32 176
                -40 76 -106 142 -181 179 -49 25 -71 29 -157 32 -73 2 -112 -1 -144 -13z m259
                -80 c73 -34 126 -86 161 -159 24 -50 29 -73 29 -135 0 -62 -5 -85 -29 -135
                -57 -119 -161 -185 -291 -185 -130 0 -234 66 -291 185 -24 50 -29 73 -29 135
                0 130 66 234 185 291 82 40 184 41 265 3z">
                    </path>
                    <path data-v-7bdd0738="" d="M545 600 c-35 -35 -68 -60 -80 -60 -27 0 -45 -18 -45 -45 0 -33 -50
                -75 -89 -75 -18 0 -41 -5 -53 -11 -20 -11 -20 -11 3 -35 12 -13 33 -24 46 -24
                17 0 23 -6 23 -23 0 -13 10 -33 23 -45 30 -28 47 -13 47 43 0 32 6 47 28 68
                15 15 37 27 48 27 26 0 44 18 44 44 0 12 26 47 60 81 l60 61 -28 27 -28 27
                -59 -60z">
                </path>
                </g>`
                            }
                        }
                        let temperatureWind_p2_svg = createDomElement(temperatureWind_p2_svg_options);

                        temperatureWind_p2.appendChild(temperatureWind_p2_svg);
                        temperatureWind_p2.innerHTML += `${dataJson.details.current.pressure}hPa`
                        temperatureWind.appendChild(temperatureWind_p1);
                        temperatureWind.appendChild(temperatureWind_p2);
                        weather_widget__content.appendChild(temperatureWind);


                        // =====humidity=====
                        let humidity_options = {
                            div: {
                                style: {
                                    width: "95%",
                                    display: "flex",
                                    justifyContent: "space-between",
                                    flexWrap: "wrap",
                                }
                            }
                        }
                        let humidity = createDomElement(humidity_options);

                        let humidity_p1_options = {
                            p: {
                                style: {
                                    fontSize: "12px",
                                    textAlign: "center",
                                    marginTop: "0",
                                },
                                innerHTML: `Humidity: ${Math.round(dataJson.details.current.humidity)}%`
                            }
                        }
                        let humidity_p1 = createDomElement(humidity_p1_options);

                        let humidity_p2_options = {
                            p: {
                                style: {
                                    fontSize: "12px",
                                    textAlign: "center",
                                    textTransform: "capitalize",
                                    marginTop: "0",
                                },
                                innerHTML: `Dew point: ${Math.round(dataJson.details.current.dew_point)}°C`
                            }
                        }
                        let humidity_p2 = createDomElement(humidity_p2_options);

                        let humidity_p3_options = {
                            p: {
                                style: {
                                    fontSize: "12px",
                                    textAlign: "center",
                                    textTransform: "capitalize",
                                    marginTop: "0",
                                },
                                innerHTML: `Visibility: ${dataJson.details.current.visibility}°C`
                            }
                        }
                        let humidity_p3 = createDomElement(humidity_p3_options);

                        humidity.appendChild(humidity_p1);
                        humidity.appendChild(humidity_p2);
                        humidity.appendChild(humidity_p3);

                        weather_widget__content.appendChild(humidity);
                        widget.appendChild(weather_widget__content);

                    });
            })
            .catch((error) => {
            })
    }

    // отрисовка блока настроек
    const renderSetting = () => {
        // структура для показа настроек
        deleteBlock('#weather-widgetMenu', "weather-widget")
        let widget = getDomElement('weather-widget');

        let weather_widgetMenu_options = {
            div: {
                style: {
                    width: "200px",
                    minHeight: "262px",
                    boxSizing: "border-box",
                    display: "flex",
                    flexDirection: "column",
                    justifyContent: "flex-start",
                    alignItems: "center",
                    padding: "10px",
                    border: "1px solid red",
                    borderRadius: "10px",
                    boxShadow: "0px 5px 10px 2px rgba(34, 60, 80, 0.2)",
                    webkitBoxShadow: "0px 5px 10px 2px rgba(34, 60, 80, 0.2)",
                    fontFamily: "Arial",
                    fontSize: "12px",
                },
                setAttribute: {
                    id: "weather-widgetMenu"
                }
            }
        }
        let weather_widgetMenu = createDomElement(weather_widgetMenu_options);

        let widgetMenu__header_options = {
            div: {
                style: {
                    maxWidth: "100%",
                    width: "100%",
                    display: "flex",
                    justifyContent: "space-between",
                },
            }
        }
        let widgetMenu__header = createDomElement(widgetMenu__header_options);

        let widgetMenu__header_p_options = {
            p: {
                style: {
                    fontSize: "16px",
                },
                innerHTML: 'Settings'
            }
        }
        let widgetMenu__header_p = createDomElement(widgetMenu__header_p_options);

        let widgetMenu__closeSettings_options = {
            i: {
                style: {
                    fontSize: "16px",
                    cursor: "pointer",
                },
                setAttribute: {
                    "id": "widgetMenu__closeSettings",
                    "aria-hidden": "true",
                },
                classList: ["fa", "fa-times"]
            }
        }
        let widgetMenu__closeSettings = createDomElement(widgetMenu__closeSettings_options);
        widgetMenu__closeSettings.addEventListener('click', async function () {
            await verificationIsConfigured()
            let weather_widget__content = await getDomElement('weather-widget__content');
            weather_widgetMenu.style.display = 'none';
            if (weather_widget__content) {
                weather_widget__content.style.display = 'flex';
            }
        });

        // при клике закрытия меню настроек - получение данных из хранилища и перерисовка окна представления
        widgetMenu__closeSettings.addEventListener("click", async function () {
            let cityList = await getLocalstorageData('cityList')
            let cityName = cityList[0].cityName;
            await renderCard(cityName);
        })
        widgetMenu__header.appendChild(widgetMenu__header_p);
        widgetMenu__header.appendChild(widgetMenu__closeSettings);
        weather_widgetMenu.appendChild(widgetMenu__header);
        widget.appendChild(weather_widgetMenu);

        let cityList = getLocalstorageData('cityList');
        cityList.forEach((item) => {
            let widgetMenu__btn_options = {
                div: {
                    style: {
                        display: "flex",
                        justifyContent: "space-between",
                        alignItems: "center",
                        maxWidth: "100%",
                        width: "calc(100% - 10px)",
                        padding: "10px 7px",
                        background: "silver",
                        textAlign: "center",
                        margin: "5px 0",
                        cursor: "grab",
                    },
                    setAttribute: {
                        "draggable": "true",
                    }
                }
            }
            let widgetMenu__btn = createDomElement(widgetMenu__btn_options);

            let dragstartHandler = (e, card) => {
                dragCity = setLocalstorageData('dragCity', card.textContent.split(",")[0])
            }
            let dragleaveHandler = (e) => {
                e.target.style.color = '#000'
            }
            let dragoverHandler = (e) => {
                e.preventDefault()
                e.target.style.color = '#ec6e4c'
            }
            let dragendHandler = (e) => {
                e.target.style.color = '#000'
            }
            let dropHandler = (e, card) => {
                dragCity = getLocalstorageData('dragCity');
                dropCity = card.textContent.split(",")[0]
                e.preventDefault()
                let cityList = getLocalstorageData('cityList')
                let newCityList = cityList.map((city) => {
                    if (city.cityName === dropCity) {
                        return {cityName: dragCity}
                    }
                    if (city.cityName === dragCity) {
                        return {cityName: dropCity}
                    }
                    return city
                })
                setLocalstorageData('cityList', newCityList);
                reRenderSetting()
            }

            widgetMenu__btn.addEventListener('dragstart', function (e) {
                dragstartHandler(e, widgetMenu__btn)
            })
            widgetMenu__btn.addEventListener('dragleave', function (e) {
                dragleaveHandler(e)
            })
            widgetMenu__btn.addEventListener('dragover', function (e) {
                dragoverHandler(e)
            })
            widgetMenu__btn.addEventListener('dragend', function (e) {
                dragendHandler(e)
            })
            widgetMenu__btn.addEventListener('drop', function (e) {
                dropHandler(e, widgetMenu__btn)
            })

            let widgetMenu__btn_i1_options = {
                i: {
                    style: {
                        fontSize: "12px",
                        cursor: "pointer",
                    },
                    setAttribute: {
                        "aria-hidden": "true",
                    },
                    classList: ["fa", "fa-bars"]
                }
            }
            let widgetMenu__btn_i1 = createDomElement(widgetMenu__btn_i1_options);

            let widgetMenu__btn_p_options = {
                p: {
                    style: {
                        display: "flex",
                        justifyContent: "space-between",
                        alignItems: "center",
                        marginBottom: "0",
                        marginLeft: "7px",
                        width: "100%",
                        fontWeight: "bold",
                    },
                    innerHTML: `${item.cityName}, `
                }
            }
            let widgetMenu__btn_p = createDomElement(widgetMenu__btn_p_options);

            // кнопка удаления города из списка в настройках
            let widgetMenu__btn_i2_options = {
                i: {
                    style: {
                        fontSize: "12px",
                        cursor: "pointer",
                    },
                    setAttribute: {
                        "aria-hidden": "true",
                        "data-sityName": item.cityName,
                    },
                    classList: ["fa", "fa-trash"]
                }
            }
            let widgetMenu__btn_i2 = createDomElement(widgetMenu__btn_i2_options);
            widgetMenu__btn_i2.addEventListener("click", function (e) {
                deleteCity(e.target);
            });

            widgetMenu__btn.appendChild(widgetMenu__btn_i1);
            widgetMenu__btn.appendChild(widgetMenu__btn_p);
            widgetMenu__btn.appendChild(widgetMenu__btn_i2);
            weather_widgetMenu.appendChild(widgetMenu__btn);

        })


        // форма добавления нового города

        if (!isConfigured) {
            console.log("isConfigured form", isConfigured)
            let subscribe_options = {
                div: {
                    style: {
                        display: "flex",
                        flexWrap: "nowrap",
                        justifyContent: "center",
                        position: "relative",
                        margin: "15px 0 25px",
                        width: "100%",
                    },
                    setAttribute: {
                        id: "subscribe_form"
                    }
                }
            }
            let subscribe = createDomElement(subscribe_options);

            let subscribe_input_options = {
                input: {
                    style: {
                        transition: "border-color 0.3s",
                        height: "35px",
                        width: "calc(100% - 30px)",
                        margin: "0",
                        fontSize: "15px",
                        boxSizing: "border-box",
                        padding: "0 10px",
                        border: "1px solid #e1e1e1",
                        borderRight: "0",
                        backgroundColor: "#fff",
                        fontFamily: "Noto Sans\",sans-serif",
                    },
                    setAttribute: {
                        type: 'text',
                        name: 'newLocation',
                        placeholder: 'Add Location',
                        required: 'required',
                    }
                }
            }
            let subscribe_input = createDomElement(subscribe_input_options);
            subscribe.appendChild(subscribe_input);

            let subscribe_btn_options = {
                button: {
                    style: {
                        transition: "border-color 0.3s",
                        width: "35px",
                        border: "0",
                        padding: "0",
                        cursor: "pointer",
                        backgroundColor: "#639",
                        fontSize: "16px",
                        height: "35px",
                        verticalAlign: "top",
                        color: "#ffffff",
                        fontFamily: 'Noto Sans",sans-serif',

                    },
                    setAttribute: {
                        type: 'text',
                        name: 'newLocation',
                        placeholder: 'Add Location',
                        required: 'required',
                    }
                }
            }
            let subscribe_btn = createDomElement(subscribe_btn_options);

            subscribe.appendChild(subscribe_btn);

            // добавление города в список
            subscribe_btn.addEventListener('click', function () {
                let text = document.getElementsByTagName("input")[0];
                let cityName = text.value;

                if (cityName) {
                    let cityList = JSON.parse(localStorage.getItem('cityList'));
                    cityList.push({cityName})
                    setLocalstorageData('cityList', cityList);
                    setLocalstorageData("isConfigured", true);

                    renderMessageBlock('')
                    reRenderSetting();
                } else {
                    renderMessageBlock("Вы не ввели название города!", 2)
                }
            })

            let subscribe_i_options = {
                i: {
                    setAttribute: {
                        "aria-hidden": 'true',
                    },
                    classList: ["fa", "fa-hand-o-left"]
                }
            }
            let subscribe_i = createDomElement(subscribe_i_options);
            subscribe_btn.appendChild(subscribe_i);

            weather_widgetMenu.appendChild(subscribe);
        }

    }

    // перерисовка блока настроек
    const reRenderSetting = async () => {
        await deleteBlock("#weather-widgetMenu", "weather-widget")
        await renderSetting();

        let widgetMenu = await getDomElement('#weather-widgetMenu');
        widgetMenu.style.display = "flex";

    }

    // удаление города из списка
    const deleteCity = async (target) => {
        let cityList = await getLocalstorageData('cityList');
        let sityName = await target.getAttribute('data-sityname');

        if (!isConfigured) {
            await cityList.forEach((item, index) => {
                if (item.cityName === sityName) {
                    cityList.splice(index, 1);
                }
            })
            await setLocalstorageData("cityList", cityList);
            await reRenderSetting();
        } else {
            await renderMessageBlock(warningMessage, 3)
        }
    }

    if (cityList && cityList.length >= 1) {
        let cityName = cityList[0].cityName;
        await verificationIsConfigured();

        if (!isConfigured) {
           await renderSetting()
        } else {
           await renderCard(cityName);
        }

    } else {
        // получение ip пользователя
        fetch(`https://api.db-ip.com/v2/free/self`)
            .then(function (resp) {
                let infoIP = resp.json()
                return infoIP;
            })

            // получение данных согласно ip пользователя
            .then(async function (infoIP) {
                let apiKey = 'ed5ebbeba257b8f262a6a9bbc0ec678e';
                let cityName = ''
                await $.getJSON('https://api.snoopi.io/' + infoIP.ipAddress + '?apikey=' + apiKey, function (data) {
                    cityName = data.City
                });

                let cityList = [];
                await cityList.push({cityName: cityName})
                await setLocalstorageData('cityList', cityList);

                return cityName

            })
            .then(async function (cityName) {
                let cityList = getLocalstorageData('cityList');

                cityName = cityList[0].cityName;
                await verificationIsConfigured()

                if (!isConfigured) {
                   await renderSetting()
                } else {
                   await renderCard(cityName);
                }
            })
    }
}